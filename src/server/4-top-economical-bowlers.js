const csv = require("csv-parser");
const fs = require("fs");
const ids = [];
let results = {};
let economyRate;

function topEconomicalBowlers(year, number) {
  if (typeof year !== "number" || typeof number !== "number") {
    return [];
  }
  fs.createReadStream("../data/matches.csv")

    .pipe(csv())

    .on("data", (data) => {
      if (parseInt(data.season) === year) {
        ids.push(parseInt(data.id));
      }
    })

    .on("end", () => {
      fs.createReadStream("../data/deliveries.csv")

        .pipe(csv())

        .on("data", (data) => {
          ids.forEach((id) => {
            if (parseInt(data.match_id) === id) {
              if (results.hasOwnProperty(data.bowler)) {
                if (
                  parseInt(data.wide_runs) === 0 &&
                  parseInt(data.noball_runs) === 0
                ) {
                  results[data.bowler][0]++;
                }
                results[data.bowler][1] +=
                  parseInt(data.total_runs) -
                  parseInt(data.legbye_runs) -
                  parseInt(data.bye_runs) -
                  parseInt(data.penalty_runs);
              } else {
                results[data.bowler] = [
                  1,
                  parseInt(data.total_runs) -
                    (parseInt(data.legbye_runs) +
                      parseInt(data.bye_runs) +
                      parseInt(data.penalty_runs)),
                ];
                if (
                  parseInt(data.wide_runs) !== 0 ||
                  parseInt(data.noball_runs) !== 0
                ) {
                  results[data.bowler][0] = 0;
                }
              }
            }
          });
        })

        .on("end", () => {
          results = Object.entries(results);

          results.forEach((data) => {
            data[1] = (data[1][1] * 6) / data[1][0];
          });

          let result = results
            .sort((a, b) => {
              return a[1] - b[1];
            })

            .slice(0, number)

            .reduce((accumulator, current) => {
              accumulator = {
                ...accumulator,
                [current[0]]: current[1].toFixed(2),
              };

              return accumulator;
            }, {});

          fs.writeFile(
            "../public/output/4-top-economical-bowlers.json",
            JSON.stringify(result),
            () => {}
          );
        });
    });
}

topEconomicalBowlers(2015, 10);
