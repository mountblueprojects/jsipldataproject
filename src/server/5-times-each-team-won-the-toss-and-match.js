const csv = require("csv-parser");
const fs = require("fs");
const result = {};

function timesEachTeamWonTheTossAndMatch() {
  fs.createReadStream("../data/matches.csv")

    .pipe(csv())

    .on("data", (data) => {
      if (data.toss_winner === data.winner) {
        if (result.hasOwnProperty(data.toss_winner)) {
          result[data.toss_winner]++;
        } else {
          result[data.toss_winner] = 1;
        }
      }
    })

    .on("end", () => {
      fs.writeFile(
        "../public/output/5-times-each-team-won-the-toss-and-match.json",
        JSON.stringify(result),
        () => {}
      );
    });
}

timesEachTeamWonTheTossAndMatch();
