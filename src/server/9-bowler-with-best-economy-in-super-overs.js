const csv = require("csv-parser");
const fs = require("fs");
let results = {};

function bowlerWithBestEconomyInSuperOvers() {
  fs.createReadStream("../data/deliveries.csv")

    .pipe(csv())

    .on("data", (data) => {
      if (parseInt(data.is_super_over) === 1) {
        if (results.hasOwnProperty(data.bowler)) {
          if (
            parseInt(data.wide_runs) === 0 &&
            parseInt(data.noball_runs) === 0
          ) {
            results[data.bowler]["balls"]++;
          }

          results[data.bowler]["runs"] +=
            parseInt(data.total_runs) -
            parseInt(data.legbye_runs) -
            parseInt(data.bye_runs) -
            parseInt(data.penalty_runs);
        } else {
          results[data.bowler] = { runs: parseInt(data.total_runs), balls: 1 };
          if (
            parseInt(data.wide_runs) !== 0 ||
            parseInt(data.noball_runs) !== 0
          ) {
            results[data.bowler]["balls"] = 0;
          }
        }
      }
    })

    .on("end", () => {
      Object.keys(results).forEach((bowler) => {
        results[bowler] = parseFloat(
          ((results[bowler]["runs"] * 6) / results[bowler]["balls"]).toFixed(2)
        );
      });

      const resultsArray = Object.entries(results);

      const result = resultsArray.reduce((highestEconomy, currentEconomy) => {
        if (currentEconomy[1] < highestEconomy[1]) {
          highestEconomy = currentEconomy;
        }

        return highestEconomy;
      }, resultsArray[0]);

      results = { [result[0]]: result[1] };

      fs.writeFile(
        "../public/output/9-bowler-with-best-economy-in-super-overs.json",
        JSON.stringify(results),
        () => {}
      );
    });
}

bowlerWithBestEconomyInSuperOvers();
