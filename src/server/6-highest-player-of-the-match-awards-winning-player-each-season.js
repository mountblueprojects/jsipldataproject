const csv = require("csv-parser");
const fs = require("fs");
let results = {};

function highestPlayerOfTheMatchAwardsWinningPlayerEachSeason() {
  fs.createReadStream("../data/matches.csv")

    .pipe(csv())

    .on("data", (data) => {
      if (results.hasOwnProperty(data.season)) {
        if (results[data.season].hasOwnProperty(data.player_of_match)) {
          results[data.season][data.player_of_match]++;
        } else {
          results[data.season][data.player_of_match] = 1;
        }
      } else {
        results[data.season] = { [data.player_of_match]: 1 };
      }
    })

    .on("end", () => {
      Object.keys(results).forEach((year) => {
        results[year] = Object.entries(results[year]);

        const playerOfTheMatch = results[year].reduce(
          (playerOfTheMatch, currentPlayer) => {
            if (currentPlayer[1] > playerOfTheMatch[1]) {
              playerOfTheMatch = currentPlayer;
            }

            return playerOfTheMatch;
          },
          results[year][0]
        );

        results[year] = { [playerOfTheMatch[0]]: playerOfTheMatch[1] };
      });

      fs.writeFile(
        "../public/output/6-highest-player-of-the-match-awards-winning-player-each-season.json",
        JSON.stringify(results),
        () => {}
      );
    });
}

highestPlayerOfTheMatchAwardsWinningPlayerEachSeason();
