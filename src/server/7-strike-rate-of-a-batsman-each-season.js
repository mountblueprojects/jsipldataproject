const csv = require("csv-parser");
const fs = require("fs");
const results = {};
const seasons = {};

function strikeRateOfBatsmanEachSeason(batsman) {
  fs.createReadStream("../data/matches.csv")

    .pipe(csv())

    .on("data", (data) => {
      seasons[data.id] = data.season;
    })

    .on("end", () => {
      fs.createReadStream("../data/deliveries.csv")

        .pipe(csv())

        .on("data", (data) => {
          if (data.batsman === batsman) {
            if (results.hasOwnProperty(seasons[data.match_id])) {
              results[seasons[data.match_id]]["runs"] += parseInt(
                data.batsman_runs
              );
              if (parseInt(data.wide_runs) === 0) {
                results[seasons[data.match_id]]["balls"]++;
              }
            } else {
              results[seasons[data.match_id]] = {
                runs: parseInt(data.batsman_runs),
                balls: 1,
              };
              if (parseInt(data.wide_runs) !== 0) {
                results[seasons[data.match_id]]["balls"] = 0;
              }
            }
          }
        })

        .on("end", () => {
          Object.keys(results).forEach((year) => {
            results[year] = (
              (results[year]["runs"] * 100) /
              results[year]["balls"]
            ).toFixed(2);
          });

          fs.writeFile(
            "../public/output/7-strike-rate-of-a-batsman-each-season.json",
            JSON.stringify(results),
            () => {}
          );
        });
    });
}

strikeRateOfBatsmanEachSeason("DA Warner");
