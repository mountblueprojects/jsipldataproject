const csv = require("csv-parser");
const fs = require("fs");
const results = {};

function matchesWonPerTeamPerYear() {
  fs.createReadStream("../data/matches.csv")

    .pipe(csv())

    .on("data", (data) => {
      if (results.hasOwnProperty(data.season)) {
        if (results[data.season].hasOwnProperty(data.winner)) {
          results[data.season][data.winner]++;
        } else {
          results[data.season][data.winner] = 1;
        }
      } else {
        results[data.season] = { [data.winner]: 1 };
      }
    })

    .on("end", () => {
      fs.writeFile(
        "../public/output/2-matches-won-per-team-per-year.json",
        JSON.stringify(results),
        () => {}
      );
    });
}

matchesWonPerTeamPerYear();
