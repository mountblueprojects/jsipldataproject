const csv = require("csv-parser");
const fs = require("fs");
const ids = [];
let results = {};

function extraRunsConcededPerTeam(year) {
  if (typeof year !== "number") {
    return [];
  }

  fs.createReadStream("../data/matches.csv")

    .pipe(csv())

    .on("data", (data) => {
      if (parseInt(data.season) === year) {
        ids.push(parseInt(data.id));
      }
    })

    .on("end", () => {
      fs.createReadStream("../data/deliveries.csv")

        .pipe(csv())

        .on("data", (data) => {
          ids.forEach((id) => {
            if (parseInt(data.match_id) === id) {
              if (results.hasOwnProperty(data.bowling_team)) {
                results[data.bowling_team] += parseInt(data.extra_runs);
              } else {
                results[data.bowling_team] = parseInt(data.extra_runs);
              }
            }
          });
        })

        .on("end", () => {
          fs.writeFile(
            "../public/output/3-extra-runs-conceded-per-team.json",
            JSON.stringify(results),
            () => {}
          );
        });
    });
}

extraRunsConcededPerTeam(2016);
