const csv = require("csv-parser");
const fs = require("fs");
const results = {};

function matchesPerYear() {
  fs.createReadStream("../data/matches.csv")

    .pipe(csv())

    .on("data", (data) => {
      if (results.hasOwnProperty(data.season)) {
        results[data.season]++;
      } else {
        results[data.season] = 1;
      }

      return results;
    })

    .on("end", () => {
      fs.writeFile(
        "../public/output/1-matches-per-year.json",
        JSON.stringify(results),
        () => {}
      );
    });
}

matchesPerYear();
