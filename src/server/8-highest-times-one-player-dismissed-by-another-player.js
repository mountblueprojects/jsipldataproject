const csv = require("csv-parser");
const fs = require("fs");
let results = {};

function highestTimesOnePlayerDismissedByAnotherPlayer() {
  fs.createReadStream("../data/deliveries.csv")

    .pipe(csv())

    .on("data", (data) => {
      if (data.player_dismissed !== "") {
        if (results.hasOwnProperty([data.player_dismissed, data.bowler])) {
          results[[data.player_dismissed, data.bowler]]++;
        } else {
          results[[data.player_dismissed, data.bowler]] = 1;
        }
      }
    })

    .on("end", () => {
      let resultsArray = Object.entries(results).sort((a, b) => {
        return b[1] - a[1];
      });

      resultsArray = resultsArray.reduce(
        (accu, curr) => {
          if (curr[0] !== accu[0][0] && curr[1] === accu[0][1]) {
            accu.push(curr);
          }

          return accu;
        },
        [resultsArray[0]]
      );

      resultsArray = resultsArray.reduce((accu, curr) => {
        let players = {
          "Dismissed Player": curr[0].split(",").slice(0, 1).join(""),
          "Dismissed by": curr[0].split(",").slice(1).join(""),
          Number: curr[1],
        };

        accu.push(players);

        return accu;
      }, []);

      fs.writeFile(
        "../public/output/8-highest-times-one-player-dismissed-by-another-player.json",
        JSON.stringify(resultsArray),
        () => {}
      );
      //   fs.writeFile("../public/output/");
    });
}

highestTimesOnePlayerDismissedByAnotherPlayer();
